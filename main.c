#include "simpleunit.h"
#include "stdio.h"

int main()
{
    int x = 10;
    create_test(some_test,
    {
        printf("Hi - %d \n", x);
        return 0;
    });
    create_test(some_other_test,
    {
        printf("Hello %d\n", *((int*)0) );
        return 0;
    });
    run(some_test);
    run(some_other_test);
}
