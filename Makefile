build: main.c library
	gcc main.c -L. -lsimpleunit -o main

run:
	LD_LIBRARY_PATH=$(shell echo $$(pwd)) ./main

library: simpleunit.c
	gcc -c -fpic -Wall -Werror simpleunit.c
	gcc -shared -o libsimpleunit.so simpleunit.o

clean:
	rm *.o libsimpleunit.so main
