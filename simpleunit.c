#include "simpleunit.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

#define BUFFER_SIZE 100

int run(testcase t)
{
    if(t.test == NULL)
    {
        return -1;
    }
    int out_pipefd[2];
    int err_pipefd[2];
    if (pipe(out_pipefd))
    {
        printf("Error stdout pipe: %d\n", errno);
        //TODO: Handle error properly
    }
    if (pipe(err_pipefd))
    {
        printf("Error stderr pipe: %d\n", errno);
        //TODO: Handle error properly
    }
    printf("Running test %s\n", t.name);
    pid_t pid = fork();
    if (pid == 0)
    {
        close(out_pipefd[0]);
        close(err_pipefd[0]);
        int std_out = out_pipefd[1];
        int std_err = err_pipefd[1];
        dup2(std_out, STDOUT_FILENO);
        dup2(std_err, STDERR_FILENO);
        close(std_out);
        close(std_err);

        int result = t.test();
        exit(result);
    }
    close(out_pipefd[1]);
    close(err_pipefd[1]);
    int std_out = out_pipefd[0];
    int std_err = err_pipefd[0];


    //TODO: Use pipe to get info.
    int status = 0;
    unsigned char return_code = 255;

    char buf[BUFFER_SIZE];
    ssize_t bytes_read;
    int out_open = 1;
    int err_open = 1;
    // TODO: Store outputs
    while ( out_open || err_open )
    {
        if (out_open)
        {
            out_open = (bytes_read = read(std_out, &buf, BUFFER_SIZE - 1)) > 0;
            if (bytes_read > 0)
            {
                buf[bytes_read] = '\0';
                printf("[std_out] %s\n", buf);
            }
        }
        if (err_open)
        {
            err_open = (bytes_read = read(std_err, &buf, BUFFER_SIZE - 1)) > 0;
            if (bytes_read > 0)
            {
                buf[bytes_read] = '\0';
                printf("[std_err] %s\n", buf);
            }
        }
    }
    close(out_pipefd[0]);
    close(err_pipefd[0]);

    // TODO: Add timeout
    waitpid(pid, &status, 0);

    if (WIFEXITED(status))
    {
        //Normal exit
        return_code = WEXITSTATUS(status);
    }
    else if (WIFSIGNALED(status))
    {
       unsigned char signal_no = WTERMSIG(status);
       int core_was_dumped = WCOREDUMP(status);
       printf("Exitted with signal: %d\n", signal_no);
       if (core_was_dumped)
       {
           printf("Core was dumped\n");
       }
    }
    else if (WIFSTOPPED(status))
    {
       unsigned char signal_no = WSTOPSIG(status);
       printf("Stopped by signal: %d\n", signal_no);
    }

    printf("Result: %d\n", return_code);
    return return_code;
}

testcase simple_unit_create_test(int (*test)(void), const char * name)
{
    testcase testcase = { test, name };
    return testcase;
}

testrun create_test_run()
{
    testrun t;
    t.testcases = NULL;
    t.count = 0;
    return t;
}

int run_all(testrun t)
{
    int errors = 0;
    for (int i = 0; i < t.count; i++)
    {
        testcase test = t.testcases[i];
        if (run(test))
        {
            errors ++;
        }
    }
    return errors;
}



//TODO: Add test-container that counts successful tests
//TODO: Add a logger that receives messages from testcases and displays them
//      based on the log level and the message
//TODO: Add option for parallel tests, i.e. don't block after fork.
